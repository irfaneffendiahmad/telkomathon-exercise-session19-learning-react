import React from 'react';
import { BrowserRouter, Switch, Route } from 'react-router-dom';
import { Helmet } from 'react-helmet';
import 'bootstrap/dist/css/bootstrap.min.css';
import './App.css';
import Navbar from './components/navbar';
import Home from './pages/AboutMe';
import ReactExample from './pages/ReactExample';
import Hooks1 from './pages/Hooks1';
import Hooks2 from './pages/Hooks2';
import Hooks3 from './pages/Hooks3';
import Hooks4 from './pages/Hooks4';
import Book from './pages/Book';
import Contributor from './pages/Contributor';
import Login from './pages/LoginPage';
import Register from './pages/Register';
import EmailFeedback from './pages/EmailFeedback';
import ProtectedRoute from './components/ProtectedRoute';
import { NotificationContainer } from 'react-notifications';
import 'react-notifications/lib/notifications.css';


function App() {

  return (

    <BrowserRouter>
      <Helmet>
        <title>Learning React</title>
      </Helmet>
      <Navbar />
      <Switch>
        <Route exact path="/" component={Home} />
        <ProtectedRoute path="/ReactExample" component={ReactExample} />
        <Route path="/Hooks1" component={Hooks1} />
        <Route path="/Hooks2" component={Hooks2} />
        <Route path="/Hooks3" component={Hooks3} />
        <Route path="/Hooks4" component={Hooks4} />
        <Route path="/Book" component={Book} />
        <ProtectedRoute path="/Contributor" component={Contributor} />
        <Route path="/EmailFeedback" component={EmailFeedback} />
        <Route path="/LoginPage" component={Login} />
        <Route path="/Register" component={Register} />
      </Switch>
      <NotificationContainer />
    </BrowserRouter>
  );
}

export default App;