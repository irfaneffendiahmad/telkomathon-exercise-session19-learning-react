import React from 'react';
import { Navbar, Container, Nav, NavDropdown } from 'react-bootstrap';
import { Link } from 'react-router-dom';


const navbar = () => {


    return (

        // Navbar menggunakan React Bootstrap
        <Navbar bg="light" expand="lg">
        <Container>
            <Navbar.Brand>Learning-React</Navbar.Brand>
            <Navbar.Toggle aria-controls="basic-navbar-nav" />
            <Navbar.Collapse id="basic-navbar-nav">
            <Nav className="me-auto">
                <Nav.Link as={Link} to="/">Home</Nav.Link>
                <Nav.Link as={Link} to="/ReactExample">Basic React JS Examples</Nav.Link>
                <NavDropdown title="Hooks Examples" id="basic-nav-dropdown">
                    <NavDropdown.Item as={Link} to="/Hooks1">Hooks 1</NavDropdown.Item>
                    <NavDropdown.Item as={Link} to="/Hooks2">Hooks 2</NavDropdown.Item>
                    <NavDropdown.Item as={Link} to="/Hooks3">Hooks 3</NavDropdown.Item>
                    <NavDropdown.Item as={Link} to="/Hooks4">Hooks 4</NavDropdown.Item>
                    <NavDropdown.Divider />
                    <NavDropdown.Item as={Link} to="/Book">Book</NavDropdown.Item>
                </NavDropdown>
                <Nav.Link as={Link} to="/Contributor">Contributor Using API Examples</Nav.Link>
                <NavDropdown title="Login & Register" id="basic-nav-dropdown">
                    <NavDropdown.Item as={Link} to="/LoginPage">Login</NavDropdown.Item>
                    <NavDropdown.Item as={Link} to="/Register">Register</NavDropdown.Item>
                    <NavDropdown.Divider />
                    <NavDropdown.Item as={Link} to="/EmailFeedback">Feedback</NavDropdown.Item>
                </NavDropdown>
            </Nav>
            </Navbar.Collapse>
        </Container>
        </Navbar>
        
    );
};

export default navbar;
