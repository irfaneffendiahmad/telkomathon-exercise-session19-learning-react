import React from 'react';
import { Redirect, Route } from 'react-router-dom';
import { connect } from 'react-redux';
import { NotificationManager} from 'react-notifications';

const ProtectedRoute = ({ isAuth, component: Component, ...rest }) => {
    return (
        <Route 
            {...rest}
            render={routeProps => {
                if(isAuth) {
                    return <Component {...routeProps} />
                } else {
                    NotificationManager.error('You are not logged in yet','',1000);
                    return (
                        <Redirect to="/" />
                    )
                }
            }}
        />
    );
};

const mapStateToProps = (state) => ({
    isAuth: state.auth.isAuthenticated
});

export default connect (mapStateToProps)(ProtectedRoute);