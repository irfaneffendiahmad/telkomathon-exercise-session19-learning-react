import React, { useState } from 'react';
import imgBook from '../assets/images/book.jpg';
import CardBook from '../components/CardBook';
import { Button } from 'reactstrap';

const Book = () => {
    const [data, setData] = useState([]);

    const handleAdd = () => {
        const newData = {
            id: data.length + 1,
            name: `Book ${data.length + 1}`,
            img: imgBook,
        }
        const newArr = [...data];
        // const result = newArr.concat(newData); // using es5
        const result = [...newArr, newData]; // using es6

        // const result = data.push(newData);
        setData(result);
    };

    const handleRemove = () => {
        const newArr = [...data];
        const result = newArr.slice(0, -1);
        // const result = newArr.pop();
        setData(result);
    }

    // console.log('data =>', data);

    return (
        <>
            <div className="App">
                <h1 style={{ backgroundColor: '#D4EFDF' }}>Add/Remove Book</h1>
                <Button color="primary" onClick={handleAdd}>Add Book</Button>
                <Button color="danger" onClick={handleRemove}>Remove Book</Button>
                </div>
            {data.map((list, index) => (
                <ul key={index}>
                    <li>
                        <CardBook
                            name={list.name}
                            // id={list.id}
                            img={list.img}
                        />
                    </li>
                </ul>
            ))}
        </>
    )
}

export default Book;