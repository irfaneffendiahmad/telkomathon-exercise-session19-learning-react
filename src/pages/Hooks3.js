import React, { useState } from 'react';
import { Button } from 'reactstrap';

const Hooks3 = () => {
    const [isNama, setIsNama] = useState(false); // jennie adalah initialState (state permulaan/ awal)
    
    const ubahNama = () => {
        setIsNama(!isNama);
    };

    console.log('isNama =>', isNama);

    return (
        <>
            <div className="App">
            <h1 style={{
            backgroundColor: '#D4EFDF'
          }}>Hooks Ketiga</h1>
            <p>Namaku adalah {isNama ? 'Jennie': 'Rose'}</p>
            <Button color="primary" onClick={ubahNama}>Ubah Nama!</Button>
            </div>
        </>
    );
};

export default Hooks3;