import React, { useState } from 'react';
import { Button } from 'reactstrap';


const ReactExample = () => {

    const [click, setClick] = useState(100);
    const [nama, setNama] = useState('Ahmad Irfan Effendi');
  
    const klikGan = () => {
      alert('Alert Test in React JS');
    }
    

    return (

      <div className="App">
        <h1 style={{
            backgroundColor: '#D4EFDF'
          }}>Basic React JS Examples</h1>
        <div className="judul">Test</div>
        <br/>
  
        <div>-------------------<br/>
        <Button color="primary" onClick={klikGan}>Click Here</Button></div>
        <br/>
  
        <div>-------------------<br/>
        Counter App<br/>
        You have clicked {click} times<br/>
        <Button color="warning" onClick={() => setClick(click + 1)}>Klik +</Button>
        <Button color="success" onClick={() => setClick(click - 1)}>Klik -</Button></div>
        <br/>
  
  
        <div>-------------------<br/>
        Complete Name:<br/>
        {nama}<br/>
        <Button color="primary" onClick={() => {setNama('Irfan')}}>Call Name!</Button></div>
        <br/>
        

      </div>
    );
}

export default ReactExample;