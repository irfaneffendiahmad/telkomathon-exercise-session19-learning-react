import React, { useState } from 'react';
import { Card } from 'react-bootstrap';
import { connect } from 'react-redux';
import { logInAction, logOutAction } from '../redux/authentication/actions';
import { useHistory } from 'react-router-dom';
import qs from 'qs';
import axios from 'axios';

const Login = (props) => {

    // -------
    const [signInData, setSignInData] = useState({
        email: '',
        password: ''
    });

    const handleChangeSignIn = (e) => {
        setSignInData({
            ...signInData,
            [e.target.name] : e.target.value
        })
    }
    // -------

    // -------
    const [data, setData] = useState({});

    const handleSubmitSignIn = (e) => {
        e.preventDefault();
        axios({
            method: 'post',
            url: 'https://reqres.in/api/register',
            data: qs.stringify({
              email: signInData.email,
              password: signInData.password,
            }),
            headers: {
              'content-type': 'application/x-www-form-urlencoded;charset=utf-8'
            }
          }).then((response) => {
            console.log('response =>', response.data.token);
            setData(response.data.token);

            if (data.token !== null) {
                localStorage.setItem('keepLogin', JSON.stringify (response.data.token)); // Keep login while page refresh
                handleClick();
            } else {
                console.log('response error', response.data.token);
            }
          });
    }

    const { isLogin, logInFunc, logOutFunc } = props;
    const history = useHistory();
    // console.log('props login page =>', props);

    const handleClick = () => {
        if (isLogin) {
            logOutFunc();
            history.push('/');
        } else {
            logInFunc();
            history.push('/ReactExample');
        }
    }

    // -------
    
    // -------
    const handleSignOut = () => {
        logOutFunc();
        history.push('/');
        localStorage.clear();
    }
    // -------


    return (
        <center>
        <Card style={{ width: '22rem' }}>
        <Card.Body>
        <form>
            <div className="App">
                <span style={{color: 'black'}}>{isLogin ? 'You are logged in' : null}</span>
                <h1 style={{backgroundColor: '#D4EFDF'}}>{isLogin ? null : 'Login'}</h1>
            </div>

            <div className="form-group" style={{textAlign: 'left'}}>
                <label>{isLogin ? null : 'Email address'}</label>
                <input 
                    type={isLogin ? 'hidden' : 'email'}
                    name="email" // Auth Key
                    className="form-control" 
                    placeholder="eve.holt@reqres.in"
                    onChange={handleChangeSignIn}
                />
            </div>

            <div className="form-group" style={{textAlign: 'left'}}>
                <label>{isLogin ? null : 'Password'}</label>
                <input 
                    type={isLogin ? 'hidden' : 'password'}
                    name="password" // Auth Key
                    className="form-control" 
                    placeholder="pistol"
                    onChange={handleChangeSignIn}
                />
            </div>

            {/* email: eve.holt@reqres.in */}
            {/* password: pistol */}

            <button 
                onClick={isLogin ? handleSignOut : handleSubmitSignIn}
                type="submit" 
                className="btn btn-primary btn-block">
                    {isLogin ? 'Logout' : 'Login'}
            </button>

            <p className="forgot-password text-right">
                <a href="/">{isLogin ? null : 'Forgot password?'}</a>
            </p>

        </form>
        </Card.Body>
        </Card>
        </center>
    );
};

const mapStateToProps = (state) => ({
    isLogin: state.auth.isAuthenticated
});

const mapDispatchToProps = (dispatch) => ({
    logInFunc: () => dispatch(logInAction),
    logOutFunc: () => dispatch(logOutAction)
});

export default connect(mapStateToProps, mapDispatchToProps)(Login);