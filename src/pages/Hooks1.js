import React, { useState } from 'react';
import { Button } from 'reactstrap';

const Hooks1 = () => {
    const [nama, setNama] = useState('Jennie');
    console.log(nama);

    const ubahNama = () => {
        if(nama === 'Jennie') {
            setNama('Rose');
        } else {
            setNama('Jennie');
        }
    };

    return (
        <>
            <div className="App">
            <h1 style={{
            backgroundColor: '#D4EFDF'
          }}>Hooks Pertama</h1>
            <p className={nama === 'Jennie' ? "ubahwarnaBlue" : "ubahwarnaRed"}>Namaku adalah {nama}</p>
            <Button color="primary" onClick={ubahNama}>Ubah Nama</Button>
            </div>
        </>
    )
}

export default Hooks1;