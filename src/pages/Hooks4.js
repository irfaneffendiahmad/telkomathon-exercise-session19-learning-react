import { useState } from 'react';
import { Button } from 'reactstrap';

const Hooks4 = () => {
    const [nama] = useState(['Jennie','Lisa','Rose','Jisoo']);
    const [idx, setIdx] = useState(0);

    const ubahNama = () => {
        if(idx<nama.length-1) {
            setIdx(idx+1);
            console.log(idx);
        } else {
            setIdx(0);
            console.log(idx);
        }
    }

    return (
        <>
            <div className="App">
            <h1 style={{
            backgroundColor: '#D4EFDF'
          }}>Hooks Keempat</h1>
            <p>Nama saya: {nama[idx]}</p>
            <Button color="primary" onClick={ubahNama}>Ubah nama</Button>
            </div>
        </>
    )
}

export default Hooks4;