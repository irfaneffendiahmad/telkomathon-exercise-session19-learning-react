import React from 'react';
import fotoProfil from '../irfan.png';
import circleFrame from '../circleframe.png';
// import logo from './logo.svg';
import '../App.css';
import '../assets/css/card.css';

const AboutMe = () => {

    return (
        <div className="App">
        <header className="App-header">

          <div className="App-parent">
            <img src={fotoProfil} className="App-logo2" alt="logo" />
            <img src={circleFrame} className="App-logo" alt="logo" />
          </div>

          <div>
            Hello!
            <br/>
            I am <span>Ahmad Irfan Effendi</span>.
            <br/>
            Telco Officer at Telkom Indonesia.

            <br/>
            <a className="App-link" style={{textDecoration: 'none'}} href="https://irfaneffendiahmad.gitlab.io">irfaneffendiahmad.gitlab.io</a>
          </div><p /><p />

          <div>
            <a
              className="App-link"
              style={{textDecoration: 'none'}}
              href="https://reactjs.org"
              target="_blank"
              rel="noopener noreferrer"
            >
              Learn React
            </a>
          </div>

        </header>
      </div>
    );
}

export default AboutMe;