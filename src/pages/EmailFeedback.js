import React,{ useState } from 'react';
import { send, init } from 'emailjs-com';
import { Card } from 'react-bootstrap';

const EmailFeedback = () => {
    const [toSend, setToSend] = useState({
      from_name: '',
      email_from: '',
      to_name: '',
      reply_to: '',
      message: '',
    });

    const [message, setMessage] = useState({});
  
    const onSubmit = (e) => {
      e.preventDefault();
      init('user_B5H0C59AACFwwyhRDue5L');
      send(
        'service_y586135',
        'template_ji8jbfn',
        toSend,
      ).then((response) => {
          console.log('SUCCESS!', response);
          setMessage(response.text);
        })
        .catch((err) => {
          console.log('FAILED...', err);
        });
    };
  
    const handleChange = (e) => {
      setToSend({ ...toSend, [e.target.name]: e.target.value });
    };
  
    return (

    <center>
    <Card style={{ width: '22rem' }}>
    <Card.Body>
        <form onSubmit={onSubmit}>
        <h1 style={{ backgroundColor: '#D4EFDF' }}>Feedback</h1>

        <div className="form-group" style={{textAlign: 'left'}}>
        <label>Your name</label>
        <input
            type='text'
            name='from_name'
            className="form-control"
            placeholder='Your name'
            value={toSend.from_name}
            onChange={handleChange}
        />
        </div>
        <div className="form-group" style={{textAlign: 'left'}}>
        <label>Your email</label>
        <input
            type='text'
            name='email_from'
            className="form-control"
            placeholder='Your email'
            value={toSend.email_from}
            onChange={handleChange}
        />
        </div>
        <div className="form-group" style={{textAlign: 'left', display: 'none'}}>
        <label>To name</label>
        <input
            type='text'
            name='to_name'
            className="form-control"
            placeholder='To name'
            value={'Ahmad Irfan Effendi'}
            // value={toSend.to_name}
            onChange={handleChange}
        />
        </div>
        <div className="form-group" style={{textAlign: 'left', display: 'none'}}>
        <label>To email</label>
        <input
            type='text'
            name='reply_to'
            className="form-control"
            placeholder='Your email'
            value={'irfaneffendiahmad@yahoo.com'}
            // value={toSend.reply_to}
            onChange={handleChange}
        />
        </div>
        <div className="form-group" style={{textAlign: 'left'}}>
        <label>Your message</label>
        <input
            type='text'
            name='message'
            className="form-control"
            placeholder='Your message'
            value={toSend.message}
            onChange={handleChange}
        />
        </div>
        <div className="App">
        <button className="btn btn-primary btn-block" type='submit'>Submit</button>
        </div>
        </form>
    </Card.Body>
    </Card>

    <p /><p />
        {Object.keys(message).length === 0 ? null : "You have sent feedback successfully!"}
    </center>

    );
  }
  export default EmailFeedback;