import React, { useState } from 'react';
import { Card } from 'react-bootstrap';
import { Link } from 'react-router-dom';
import qs from 'qs';
import axios from 'axios';

const Register = () => {

    // -------
    const [regData, setRegData] = useState({
        email: '',
        password: ''
    });

    const [data, setData] = useState({});

    const handleChangeReg = (e) => {
        setRegData({
            ...regData,
            [e.target.name] : e.target.value
            // Jadinya sbb => password: yg user ketik | email: yg user ketik
        })
    }
    // -------

    // -------
    const handleSubmitReg = (e) => {
        e.preventDefault();
        axios({
            method: 'post',
            url: 'https://reqres.in/api/register',
            data: qs.stringify({
              email: regData.email,
              password: regData.password,
            }),
            headers: {
              'content-type': 'application/x-www-form-urlencoded;charset=utf-8'
            }
          }).then((response) => {
              console.log('response =>', response);
              setData(response.data);
          })
    }
    // console.log('regData =>', regData);
    // -------


    return (
        <center>
        <Card style={{ width: '22rem' }}>
        <Card.Body>
        <form>
            <h1 style={{ backgroundColor: '#D4EFDF' }}>Registration</h1>

            <div className="form-group" style={{textAlign: 'left'}}>
                <label>Email address</label>
                <input 
                    type="email" 
                    placeholder="eve.holt@reqres.in"
                    name="email"
                    className="form-control"
                    onChange={handleChangeReg} />
            </div>

            <div className="form-group" style={{textAlign: 'left'}}>
                <label>Password</label>
                <input 
                    type="password"
                    placeholder="pistol"
                    name="password"
                    className="form-control"
                    onChange={handleChangeReg} />
            </div>

            {/* email: eve.holt@reqres.in */}
            {/* password: pistol */}

            <button 
                onClick={handleSubmitReg}
                type="submit" 
                className="btn btn-primary btn-block">
                    Register
            </button>

            <p className="forgot-password text-right">
                Already registered <Link to="/LoginPage">sign in?</Link>
            </p>
        </form>
        </Card.Body>
        </Card>

        <p /><p />
        {Object.keys(data).length === 0 ? null : "You have registered successfully with id "}{data.id}
        </center>
    );
};

export default Register;
