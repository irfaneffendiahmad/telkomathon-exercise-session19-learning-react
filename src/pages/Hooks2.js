import React, { useState } from 'react';
import { Button } from 'reactstrap';

const Hooks2 = () => {
    const [isNama, setIsNama] = useState(true); // jennie adalah initialState (state permulaan/ awal)
    const [name, setName] = useState('Jennie')
    console.log(name);
    
    const ubahNama = () => {
        if (isNama) {
            setIsNama(false)
            setName('Rose');
        } else {
            setIsNama(true);
            setName('Jennie');
        }
    };

    return (
        <>
            <div className="App">
            <h1 style={{
            backgroundColor: '#D4EFDF'
          }}>Hooks Kedua</h1>
            <p>Namaku adalah {name}</p>
            <Button color="primary" onClick={ubahNama}>Ubah Nama!</Button>
            </div>
        </>
    );
};

export default Hooks2;