import { LOG_IN, LOG_OUT } from './actionTypes';
import { NotificationManager} from 'react-notifications';

const initialState = {
    // isAuthenticated: false,
    isAuthenticated: localStorage.getItem("keepLogin"),// Activate login after page refresh
};

const authReducer = (state = initialState, action) => {
    if (action.type === LOG_IN) {
        NotificationManager.success('You are logged in','',1000);
        return {
            ...state,
            isAuthenticated: true,
        }
    } else if (action.type === LOG_OUT) {
        NotificationManager.warning('You have logged out successfully','',1000);
        return {
            ...state,
            isAuthenticated: false,
        }
    } 
    return state;
};

export default authReducer;